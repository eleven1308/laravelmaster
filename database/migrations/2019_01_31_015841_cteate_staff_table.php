<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CteateStaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
           $table->increments('id');
           $table->string('name');
           $table->string('email')->unique();
           $table->string('password');
           $table->tinyInteger('changed_password');
           $table->string('address')->nullable();
           $table->tinyInteger('gender')->default(1);
           $table->string('username');
           $table->tinyInteger('is_root');
           $table->string('token');
           $table->tinyInteger('status');
           $table->unsignedInteger('position_id');
           $table->foreign('position_id')->references('id')->on('position');
           $table->unsignedInteger('department_id');
           $table->foreign('department_id')->references('id')->on('department');
           $table->dateTime('created_at');
           $table->dateTime('modified_at');
           $table->dateTime('login_at');
       });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('staff');


    }
}
