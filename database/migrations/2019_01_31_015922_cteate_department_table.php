<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CteateDepartmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('department', function (Blueprint $table) {
          $table->increments('id');
          $table->string('name');
          $table->integer('sort_order');
          $table->dateTime('created_at');
          $table->dateTime('modified_at');

      });

    }  

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    Schema::drop('department');

    }
}
